def bmr():
    print("请输入以下信息，用空格分隔")
    input_str = input("性别 体重(kg) 身高(cm) 年龄:")
    str_list = input_str.split(' ')
    try:
        gender = str_list[0]
        weight = float(str_list[1])
        height = float(str_list[2])
        age = int(str_list[3])
        if gender == "男":
            BMR = 66 + (13.7 * weight) + (5.0 * height) - (6.8 * age)
        elif gender == "女":
            BMR = 655 + (9.6 * weight) + (1.8 * height) - (4.7 * age)
        else:
            BMR = -1
            print("请重新输入正确的信息")
            return bmr()

        if BMR != -1:
            print("您的基础代谢率:{:.2f}大卡".format(BMR))
    except ValueError:
        print("请重新输入正确的信息")
        return bmr()
    except IndexError:
        print("输入的信息太少")
        return bmr()
    except:
        print("程序异常")
        return bmr()

while True:
    y_or_n = input("是否退出程序(y/n)?:")
    if y_or_n == 'y':
        print("程序已结束")
        break
    elif y_or_n == 'n':
        bmr()
        print()
    else:
        print("指令有误，请重新输入")